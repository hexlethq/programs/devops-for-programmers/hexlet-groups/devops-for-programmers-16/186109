data "digitalocean_ssh_key" "default" {
  name = "default"
}

resource "digitalocean_vpc" "default" {
  name     = "vpc-security-homework"
  region   = "ams3"
  ip_range = "10.10.10.0/24"
}

resource "digitalocean_droplet" "bastion" {
  image    = "docker-20-04"
  name     = "bastion-security-homework"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  vpc_uuid = digitalocean_vpc.default.id
  ssh_keys = [data.digitalocean_ssh_key.default.id]
}

resource "digitalocean_droplet" "web_01" {
  image    = "docker-20-04"
  name     = "web-security-homework-01"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  vpc_uuid = digitalocean_vpc.default.id
  ssh_keys = [data.digitalocean_ssh_key.default.id]
}

resource "digitalocean_droplet" "web_02" {
  image    = "docker-20-04"
  name     = "web-security-homework-02"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  vpc_uuid = digitalocean_vpc.default.id
  ssh_keys = [data.digitalocean_ssh_key.default.id]
}
