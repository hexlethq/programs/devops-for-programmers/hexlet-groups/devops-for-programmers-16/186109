resource "digitalocean_domain" "default" {
  name = var.domain_name
}

resource "digitalocean_record" "security" {
  domain = digitalocean_domain.default.name
  type   = "A"
  name   = "security"
  value  = digitalocean_loadbalancer.lb.ip
}

resource "digitalocean_certificate" "default" {
  name    = "default"
  type    = "lets_encrypt"
  domains = ["security.${digitalocean_domain.default.id}"]
}

resource "digitalocean_loadbalancer" "lb" {
  name   = "lb-security-homework"
  region = "ams3"
  vpc_uuid = digitalocean_vpc.default.id

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = var.app_port
    target_protocol = "http"
  }

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"

    target_port     = var.app_port
    target_protocol = "http"

    certificate_name = digitalocean_certificate.default.name
  }

  healthcheck {
    port     = var.app_port
    protocol = "http"
    path     = "/"
  }

  droplet_ids = [
    digitalocean_droplet.web_01.id,
    digitalocean_droplet.web_02.id
  ]
}
