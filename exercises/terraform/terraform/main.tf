provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "default" {
  name = "default"
}

resource "digitalocean_droplet" "web_terraform_homework_01" {
  image    = "docker-20-04"
  name     = "web-terraform-homework-01"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.default.id]
}

resource "digitalocean_droplet" "web_terraform_homework_02" {
  image    = "docker-20-04"
  name     = "web-terraform-homework-02"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.default.id]
}

resource "digitalocean_loadbalancer" "lb_terraform_homework" {
  name   = "lb-terraform-homework"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = var.app_port
    target_protocol = "http"
  }

  healthcheck {
    port     = var.app_port
    protocol = "http"
    path     = "/"
  }

  droplet_ids = [
    digitalocean_droplet.web_terraform_homework_01.id,
    digitalocean_droplet.web_terraform_homework_02.id
  ]
}

resource "digitalocean_domain" "default" {
  name       = var.domain_name
  ip_address = digitalocean_loadbalancer.lb_terraform_homework.ip
}

output "droplets_ip_addr" {
  value = [
    digitalocean_droplet.web_terraform_homework_01.ipv4_address,
    digitalocean_droplet.web_terraform_homework_02.ipv4_address
  ]
}
