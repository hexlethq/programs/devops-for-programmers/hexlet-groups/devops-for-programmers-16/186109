variable "do_token" {
  type = string
}

variable "domain_name" {
  type = string
}

variable "app_port" {
  type    = number
  default = 5000
}
